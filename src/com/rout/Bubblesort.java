package com.rout;

/**
 * Bubble sort - Moves from left to right, compares two adjacent values and switch
 * values so that greatest values move to right hand side.
 */
public class Bubblesort {

    public static void main(String[] args) {

        // unsorted data set
        int[] unsortedArray = {12, 8, 7, 5, 2};

        int tmp;
        for (int i = 0; i < unsortedArray.length - 1; i++) {

            for (int j = 0; j < unsortedArray.length - 1 - i; j++) {

                if (unsortedArray[j] > unsortedArray[j + 1]) {
                    tmp = unsortedArray[j + 1];
                    unsortedArray[j + 1] = unsortedArray[j];
                    unsortedArray[j] = tmp;
                }
            }
        }

        for (int i = 0; i < unsortedArray.length; i++) {
            System.out.println(unsortedArray[i]);
        }
    }
}
