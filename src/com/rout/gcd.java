package com.rout;

import java.util.Scanner;

/**
 * Greatest Common Divisor
 */
public class gcd {

    public static void main(String[] args) {

        System.out.println("gcd.main");
        System.out.println("Find Greatest common divisor");
        Scanner scanner = new Scanner(System.in);

        System.out.println("First value");

        int firstValue = scanner.nextInt();

        System.out.println("Second value");

        int secondValue = scanner.nextInt();
        int gcd;

        int dividend = firstValue;
        int divisor = secondValue;

        while (true) {

            int remainder = dividend % divisor;

            if (remainder == 0) {
                gcd = divisor;
                break;
            }

            dividend = divisor;
            divisor = remainder;
        }

        System.out.printf("GCD of %d and %d is %d %n", firstValue, secondValue, gcd);
    }
}
